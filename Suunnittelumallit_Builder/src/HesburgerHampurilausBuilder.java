
public class HesburgerHampurilausBuilder extends HampurilaisBuilder{
	
	private HesburgerHampurilainen hampurilainen = new HesburgerHampurilainen();

	@Override
	public void rakennaPihvi() {
		hampurilainen.setPihvi(new Kasvispihvi());
		
	}

	@Override
	public void rakennaVihannes() {
		hampurilainen.setSalaatti(new Salaatti());
		hampurilainen.setTomaatti(new Tomaatti());
		
	}

	@Override
	public void rakennaMajoneesi() {
		hampurilainen.setMajoneesi(new Majoneesi());
		
	}

	@Override
	public void rakennaSämpylä() {
		hampurilainen.setSämpylä(new Sesamsiemensämpylä());
		
	}
	
	public void getHampurilainen() {
		System.out.println("Hesburgerin hampurilaiseen kuuluu:");
		for (int i = 0; i < hampurilainen.esitteleHampurilainen().size(); i++) {
			System.out.print(hampurilainen.esitteleHampurilainen().get(i));
			
	}
	}

}
