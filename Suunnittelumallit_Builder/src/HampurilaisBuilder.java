
public abstract class HampurilaisBuilder {
	
	public abstract void getHampurilainen();
	
	public abstract void rakennaPihvi();
	public abstract void rakennaVihannes();
	public abstract void rakennaMajoneesi();
	public abstract void rakennaSämpylä();

}
