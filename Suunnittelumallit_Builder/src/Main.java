
public class Main {

	public static void main(String[] args) {
		
		Työntekijä työntekijä = new Työntekijä();
		HampurilaisBuilder hesburgerHampurilainenb = new HesburgerHampurilausBuilder();
		HampurilaisBuilder mcdonaldsHampurilainenb = new McDonaldsHampurilaisBuilder();
		
		//työntekijä.setHamppariBuilder(hesburgerHampurilainenb);
		työntekijä.setHamppariBuilder(mcdonaldsHampurilainenb);
		työntekijä.rakennaHampurilainen();
		
		//hesburgerHampurilainenb.getHampurilainen();
		mcdonaldsHampurilainenb.getHampurilainen();

	}

}
