
public class McDonaldsHampurilaisBuilder extends HampurilaisBuilder{
	
	public McDonaldsHampurilainen hampurilainen = new McDonaldsHampurilainen();

	@Override
	public void rakennaPihvi() {
		hampurilainen.setPihvi(new Halloum());
		
	}

	@Override
	public void rakennaVihannes() {
		hampurilainen.setSalaatti(new Salaatti());
		hampurilainen.setKurkku(new Suolakurkku());
		
	}

	@Override
	public void rakennaMajoneesi() {
		hampurilainen.setMajoneesi(new Majoneesi());
		
	}

	@Override
	public void rakennaSämpylä() {
		hampurilainen.setSämpylä(new Täysjyväsämpylä());
		
	}

	@Override
	public void getHampurilainen() {
		System.out.println("McDonaldsin hampurilaiseen kuuluu:");
		System.out.println(hampurilainen.esitteleHampurilainen().toString());
		
	}

}
