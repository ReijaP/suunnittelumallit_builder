
public class Työntekijä {
	private HampurilaisBuilder hampparibuilder;
	
	public void setHamppariBuilder(HampurilaisBuilder hb) {
		this.hampparibuilder = hb;
	}
	
	public void rakennaHampurilainen() {
		hampparibuilder.rakennaSämpylä();
		hampparibuilder.rakennaPihvi();
		hampparibuilder.rakennaVihannes();
		hampparibuilder.rakennaMajoneesi();
	}

}
