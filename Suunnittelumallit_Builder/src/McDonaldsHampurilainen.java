
public class McDonaldsHampurilainen {
	
	private StringBuilder mcDonaldsHampurilainen = new StringBuilder();
	
	public void setSämpylä(Täysjyväsämpylä sämpylä) {
		this.mcDonaldsHampurilainen.append(sämpylä);
	}
	
	public void setPihvi(Halloum halloum) {
		this.mcDonaldsHampurilainen.append(halloum);
	}
	
	public void setSalaatti(Salaatti salaatti) {
		this.mcDonaldsHampurilainen.append(salaatti);
	}
	
	public void setKurkku(Suolakurkku suolakurkku) {
		this.mcDonaldsHampurilainen.append(suolakurkku);
	}
	
	public void setMajoneesi(Majoneesi majoneesi) {
		this.mcDonaldsHampurilainen.append(majoneesi);
	}
	
	public StringBuilder esitteleHampurilainen() {
		return mcDonaldsHampurilainen;
	}

}
