import java.util.ArrayList;
import java.util.List;

public class HesburgerHampurilainen {
	
	private List<Täyte> hesburgerHampurilainen = new ArrayList<>();
	
	public void setSämpylä(Sesamsiemensämpylä sämpylä) {
		this.hesburgerHampurilainen.add(sämpylä);
	}
	
	public void setPihvi(Kasvispihvi pihvi) {
		this.hesburgerHampurilainen.add(pihvi);
	}
	
	public void setSalaatti(Salaatti salaatti) {
		this.hesburgerHampurilainen.add(salaatti);
	}
	
	public void setTomaatti(Tomaatti tomaatti) {
		this.hesburgerHampurilainen.add(tomaatti);
	}
	
	public void setMajoneesi(Majoneesi majoneesi) {
		this.hesburgerHampurilainen.add(majoneesi);
	}
	
	public List<Täyte> esitteleHampurilainen() {
		return hesburgerHampurilainen;
	}

}
